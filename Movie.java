public class Movie {
	
	private String name;
    private String producer;
	private int yearOfRelease;
	
	public Movie(String name, String producer, int yearOfRelease){
		this.name = name;
		this.producer = producer;
		this.yearOfRelease = yearOfRelease;
		
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setName(String newName){
		this.name = newName;
	}

	public String getProducer(){
		return this.producer;
	}
	
	public void setProducer(String newProducer){
		this.producer = newProducer;
	}
	
	public int getYearOfRelease(){
		return this.yearOfRelease;
	}
	
	public void setYearOfRelease(int newYearOfRelease){
		this.yearOfRelease = newYearOfRelease;
	}
	

	public void favoriteMovie(){
		System.out.println("My favorite movie is " + name);
		
	}

	
	
}